from django.conf.urls import url
from django.conf.urls.static import static
from tuenergiarepsol.views import *

#  from django.views.generic.base import RedirectView
from django.views.generic import TemplateView, RedirectView

urlpatterns = [

    url(r'^$', login),
    url(r'^login/$', login, name="login"),  # action form-html
    url(r'^estaciones/$', login, name="estaciones"),  # 1 de Marzo
    url(r'^stands/$', login, name="stands"),  # 8 de Marzo o Landing Stands
    url(r'^mobile/$', login, name="mobile"),  # 8 de Marzo o Landing Stands
    url(r'^body/?$', body),
    url(r'^send/$', send),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)