"""
WSGI config for tuenergiarepsol_estaciones project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/howto/deployment/wsgi/
"""

import os
import sys

# lo adelanto para las turas estaticas de los archivos

# sys.path.append('/var/www/repsol/')
from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'tuenergiarepsol.settings')

application = get_wsgi_application()
