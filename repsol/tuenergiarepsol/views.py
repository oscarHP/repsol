from django.core.files.base import ContentFile
from django.http import HttpResponse
from django.template import loader
from django.shortcuts import redirect
from django.conf import settings
import hashlib


def login(request):
    message = ''
    if 'user' in request.POST and 'password' in request.POST:
        user = request.POST['user']
        user_bln = False
        password = request.POST['password']
        password_bln = False

        if user is not None:
            user_bln = True
        else:
            message = 'Debe introducir el nombre de usuario'

        if password is not None:
            password_bln = True
        else:
            message = 'Debe introducir el password de usuario'

        if user_bln and password_bln and password == settings._PASSWORD and user == settings._USER:
            response = redirect('/body/?landing=estaciones')
            response.set_cookie('cookie_t2o', hashlib.sha224(password.encode('utf-8')).hexdigest(), max_age=1000)
            return response
    else:
        message = 'Inserte los datos requeridos'

    template = loader.get_template('login.html')
    context = {'message': request.path.replace('/', '')}
    #  request.session['slug'] = request.path.replace('/', '')
    return HttpResponse(template.render(context, request))


def body(request):

    cookie_t2o = request.COOKIES.get('cookie_t2o')

    if cookie_t2o != hashlib.sha224(settings._PASSWORD.encode('utf-8')).hexdigest():
        template = loader.get_template('login.html')
        message = 'Error en la conexion'
        context = {'message': message}
        return HttpResponse(template.render(context, request))

    message = ''
    if 'landing' in request.GET:
        print("Hola")
    template = loader.get_template('estaciones.html')
    context = {'mensaje': message}
    return HttpResponse(template.render(context, request))


def send(request):
    message = ''
    string_to_return = request.FILES['facturas'].read()
    file_to_send = ContentFile(string_to_return)
    template = loader.get_template('gracias.html')
    context = {'message': message}
    return HttpResponse(template.render(context, request))